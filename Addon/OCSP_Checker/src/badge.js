/**
 * Affiche que le site n'utilise pas OCSP Stapling
 */
export function setNone() {
    chrome.browserAction.setBadgeText({ text: '' })
    chrome.browserAction.setBadgeBackgroundColor({ color: [0, 0, 0, 0] })
}

/**
 * Affiche que est suivi et que son attestation est depassee
 */
export function showWarning() {
    chrome.browserAction.setBadgeText({ text: '!!!' })
    chrome.browserAction.setBadgeBackgroundColor({ color: 'red' })
}

/**
 * Affiche que le site est suivi et que son attestation est bonne
 */
export function showOk() {
    chrome.browserAction.setBadgeText({ text: 'Ok' })
    chrome.browserAction.setBadgeBackgroundColor({ color: 'green' })
}

/**
 * Affiche que le site utilise OCSP Stapling mais
 * n'est pas suivi
 */
export function showUnfollowed() {
    chrome.browserAction.setBadgeText({ text: '?' })
    chrome.browserAction.setBadgeBackgroundColor({ color: 'orange' })
}