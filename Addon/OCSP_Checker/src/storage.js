/**
 * Vérifie que le localstorage soit disponible
 * @returns {Boolean}
 */
export function isAvailable() {
    try {
        let storage = window['localStorage'];
        let x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch (e) {
        return false;
    }
}

export function isUnavailableError() {
    console.error('[ error ] The local storage isn\'t available.');
}

/**
 * Ajoute un site à la liste
 * @param {String} hostname - Nom du site
 * @param {String} time - Durée limite
 */
export function addSite(hostname, time) {
    if (isAvailable()) {
        localStorage.setItem(hostname, time);
    } else {
        isUnavailableError()
    }
}

/**
 * Récupère l'ancienneté pour un site stocké en secondes
 * @param {String} hostname - Nom du site à récuperer
 */
export function getCriticalSeconds(hostname) {
    if (isAvailable()) {
        let critical = localStorage.getItem(hostname);
        if (critical == null) {
            return null;
        }
        return critical
    } else {
        isUnavailableError()
        return null;
    }
}


/**
 * Supprime un site de la liste
 * @param {String} hostname
 */
export function removeSite(hostname) {
    if (isAvailable()) {
        localStorage.removeItem(hostname);
    } else {
        isUnavailableError();
    }
}

/**
 * Vide la liste
 */
export function removeAllSites() {
    if (isAvailable()) {
        localStorage.clear();
    } else {
        isUnavailableError();
    }
}

/**
 * Donne le nombre de sites stockés
 * @returns {Number}
 */
export function getLength() {
    return localStorage.length
}

/**
 * Retourne le nom d'hôte pour une position
 * donnée dans la liste
 * @param {Number} i - Position du site dans la liste
 */
export function getHostname(i) {
    return localStorage.key(i)
}

/**
 * Vérifie si la liste est vide
 * @returns {Boolean}
 */
export function isEmpty() {
    return localStorage.length === 0
}

/**
 * Vérifie qu'un site soit suivi
 * @param {String} hostname
 * @returns {Boolean}
 */
export function isFollowed(hostname) {
    return getCriticalSeconds(hostname) != null
}