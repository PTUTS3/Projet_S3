import { getTabs, getHostname } from './url'
import { sendNative, sendBackground } from './messaging'
import { treatUpdate, isDate } from './date'
import { isFollowed, getCriticalSeconds } from './storage'
import * as badge from './badge'

// Par defaut ne pas afficher de badge
badge.setNone()

/**
 * Variable accessible à tout le fichier
 * permet de ne pas refaire la verification
 * à chaque fois qu'on navigue sur le site
 * let serveur_precedent
 */
let serveur_precedent

/**
 * Envoie un message au content si
 * la limite est dépassée
 * @param update - date de la mise à jour
 * @param hostname - hostname du site
 * @param tabId - ID de l'onglet courant
 * @returns {boolean} - Faux si l'ancienneté est trop importante
 */
function checkUpdate(update, hostname, tabId) {
    // Si le site n'utilise pas OCSP Stapling
    // ne pas afficher de badge et arreter le traitement
    if (!update || update === 'NO_STAPLING') {
        badge.setNone()
        return
    }
    let followed = isFollowed(hostname)
    if (!followed) {
        // Si le site n'est pas suivi, le montrer
        badge.showUnfollowed()
        return false
    }
    if (followed) {
        // Calculer le depassement
        let dep = treatUpdate(update, getCriticalSeconds(hostname))
        // Si il y a depassement, afficher l'avertissement
        if (dep) {
            // Si il y a depassement, montrer un badge rouge
            badge.showWarning()
            // Renseigner le site courant dans l'objet a renvoyer
            dep.hostname = hostname
            // Renvoyer l'objet depassement
            return dep
        } else {
            // Sinon montrer que tout est bon
            badge.showOk()
            let rep = {
                depas: false,
                hostname: hostname
            }
            return rep
        }
    }
}

/**
 * @param {Port} port - port de connexion a la popup
 * @param {Object} message - message recu de la popup
 */
async function onPopupMessage(port, message) {
    let response = await sendNative(message)
    switch (message.action) {
        case 'check_stapling':
            port.postMessage({
                has_stapling: isDate(response.update),
                hostname: message.hostname
            })
            break
        case 'get_date':
            let update = checkUpdate(response.update, message.hostname)
            if (!update.depas && message.manual) {
                // Si l'anciennete n'est pas depassee mais que la popup
                // a demande une verification manuelle, lui renvoyer
                // une confirmation
                port.postMessage({
                    show_ok: true,
                    hostname: message.hostname
                })

            } else {
                // Si l'anciennete est depassee, notifier la popup
                port.postMessage(update)
            }
            break
        case 'unfollow':
            badge.showUnfollowed()
            break
    }
}

// Quand la popup se connecte
chrome.runtime.onConnect.addListener(port => {
    // Attendre un message de sa part
    port.onMessage.addListener(message => {
        onPopupMessage(port, message)
    })
})

/**
 * Récupère l'url de l'onglet courant, l'envoie à l'application
 * native puis delegue la verification de la reponse a check_update
 * @param {Integer} tabId - Identifiant de l'onglet charge
 * @param {String} changeInfo - Informations sur l'etat de l'onglet
 */
async function queryCurrentSite(tabId, loading_status) {
    // Attendre la fin du chargement de la page
    if (loading_status !== 'complete') return
    // Recuperer les onglets
    let tabs = await getTabs()
    // Ne faire le traitement que pour l'onglet actif
    if (tabs[0].id !== tabId) return
    let hostname = getHostname(tabs[0].url)
    // Verifier qu'on etait pas deja sur le site
    if (serveur_precedent === hostname) return
    // Sauvegarder le site courant
    serveur_precedent = hostname
    let response = await sendNative({ hostname: hostname })
    checkUpdate(response.update, hostname, tabId)
}

// A chaque fois qu'un onglet est chargé
chrome.tabs.onUpdated.addListener((tabId, changeInfo) => {
    queryCurrentSite(tabId, changeInfo.status)
})

// Surveiller le changement d'onglet
chrome.tabs.onActivated.addListener(tabInfo => {
    queryCurrentSite(tabInfo.tabId, 'complete')
})