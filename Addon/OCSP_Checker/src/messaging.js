// Identifiant de l'application native
const id_app = 'com.e2.ocsp_checker'

/**
 * Envoie un message à l'application native
 * @param {Object} message - message à envoyer
 * @returns {Promise} - Date de la mise à jour
 */
export function sendNative(message) {
    return new Promise((resolve) => {
        chrome.runtime.sendNativeMessage(id_app, message, resolve)
    })
}