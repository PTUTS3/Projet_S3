import { asMilliseconds, toObject } from './date'
import { getCurrentHostname } from './url'
import { isFollowed, getCriticalSeconds, addSite, removeSite, getLength } from './storage'
import * as badge from './badge'
import * as ui from './ui'

/* Variables globales */
let siteToModif
let hostname
let nativeConnected = false

// Timeout de 2 secondes
setTimeout(() => {
    // Si on est pas connecte au bout de 2 secondes
    // afficher une erreur dans la fenetre et dans la console
    if (!nativeConnected) ui.showConnectionError()
    throw new Error("Impossible de se connecter à l'application native")
}, 2000)

/**
 * Recupere le nom d'hote du site 
 * courant et l'enregistre globalement
 * puis met jour l'etat de la page
 */
async function refresh() {
    hostname = await getCurrentHostname()
    // Quand on charge la page
    // afficher la liste des sites
    ui.printSites()
    // Puis verifier l'état du site
    updateSiteStatus()
}
refresh()

// Se connecter au backgound
const port = chrome.runtime.connect()

/**
 * Ecouter les reponses/messages envoyes par le background
 */
port.onMessage.addListener(message => {
    nativeConnected = true
    ui.showListButton()
    // Dès qu'on reçoit la réponse, cacher l'écran de chargement
    ui.hideLoading()
    // Si le suite est suivi, verifier la maj
    if (isFollowed(message.hostname)) {
        if (message.depas) {
            // Si il y a un depassement, afficher l'avertissement
            ui.showDepInfo(message.maj, message.depas)
        } else {
            // Sinon afficher une confirmation
            if (message.show_ok) ui.showOkIcon()
            ui.showFollowed()
        }
    } else {
        // Sinon si il utilise OCSP Stapling
        // proposer de le suivre
        if (message.has_stapling) {
            ui.showUnfollowed()
        } else {
            // Afficher avertissement s'il ne l'utilise pas
            ui.showDisabled()
        }
    }
})

/**
 * Met à jour l'état (suivi/non suivi/pas supporté) du
 * site courant
 */
function updateSiteStatus() {
    // Si le site est deja suivi reverifier la mise a jour
    if (isFollowed(hostname)) {
        port.postMessage({
            hostname: hostname,
            action: 'get_date',
            lim: getCriticalSeconds(hostname)
        })
    } else {
        // Sinon verifier qu'il supporte OCSP Stapling
        port.postMessage({
            hostname: hostname,
            action: 'check_stapling'
        })
    }
}

// Quand on clique sur "Suivre"
ui.btn_follow.addEventListener('click', () => {
    let days = document.getElementById('days').value;
    let hours = document.getElementById('hours').value;
    let mins = document.getElementById('minutes').value;
    let secs = document.getElementById('seconds').value;
    if (!valid_modif(days, hours, mins, secs)) {
        ui.showAddWarning();
        return
    }
    // Stocker la durée un millisecondes
    let time = asMilliseconds(days, hours, mins, secs)
    // Ajouter le site dans la liste des suivis
    addSite(hostname, time)
    // Envoyer une requête au background pour forcer la vérification
    // avec l'adresse du serveur concerné et la duree limite
    port.postMessage({
        hostname: hostname,
        action: 'get_date',
        lim: time
    })
    ui.printSites()
})

// Quand on clique sur "Ne plus suivre"
ui.btn_unfollow.addEventListener('click', () => {
    ui.unfollow(hostname)
    port.postMessage({
        hostname: hostname,
        action: 'unfollow'
    })
})

// Quand on clique sur "Vider la liste des sites"
ui.btn_unfollowall.addEventListener('click', () => {
    ui.unfollowAllSites()
    port.postMessage({
        hostname: hostname,
        action: 'unfollow'
    })
    // Actualiser la popup en fonction de la page ou on se trouve
    updateSiteStatus()
    ui.hideList()
})

// Quand on clique sur la fenêtre
document.addEventListener('click', e => {

    // Si on a cliqué sur l'icone de modification d'un site
    if (e.target.classList.contains('edit_site')) {
        modifySite(e.target.id)
    }

    // Si on a cliqué sur l'icone de suppression d'un site
    if (e.target.classList.contains('delete_site')) {
        removeFromList(e.target.id)
    }
})

/**
 * Affiche l'interface de modification d'un site de la liste
 * @param {String} id - Identifiant du site clique dans la liste
 */
function modifySite(id) {
    let time = getCriticalSeconds(id)
    let obj_date = toObject(time)
    document.getElementById('days_modif').value = obj_date.days
    document.getElementById('hours_modif').value = obj_date.hours
    document.getElementById('minutes_modif').value = obj_date.minutes
    document.getElementById('seconds_modif').value = obj_date.seconds
    // Afficher les champs de modification
    ui.showModif()
    siteToModif = id;
}

/**
 * Supprimer un site de la liste
 * @param {String} id - Identifiant du site a supprimer
 */
function removeFromList(id) {
    removeSite(id);
    if (hostname === id) ui.showUnfollowed()
    port.postMessage({
        hostname: hostname,
        action: 'unfollow'
    })
    if (getLength() === 0) ui.hideList()
    ui.printSites();
}

// Quand on clique sur "Développer" pour afficher la liste des sites
ui.btn_chevron_down.addEventListener('click', () => {
    ui.showList()
})

// Quand on clique sur "Réduire" pour cacher la liste des sites
ui.btn_chevron_up.addEventListener('click', () => {
    ui.hideList()
})

// Quand on clique sur "Modifier" pour modifier le site concerné
ui.btn_modif_done.addEventListener('click', () => {
    let days = document.getElementById('days_modif').value;
    let hours = document.getElementById('hours_modif').value;
    let mins = document.getElementById('minutes_modif').value;
    let secs = document.getElementById('seconds_modif').value;

    // Si les valeurs à modifier sont correctes
    if (valid_modif(days, hours, mins, secs)) {
        let time = asMilliseconds(days, hours, mins, secs)
        addSite(siteToModif, time)
        // Refaire une vérification auprès du background avec la nouvelle date si le site modifié est le site visité
        if (hostname === siteToModif) {
            port.postMessage({
                hostname: hostname,
                action: 'get_date',
                lim: time
            })
            ui.printSites()
            // Rafficher l'écran précedent
            updateSiteStatus()
        }
    } else {
        ui.showModifWarning();
    }
})

// Retourne vrai si ce sont des entiers positifs, avec days < 24 et hours, mins et secs < 60
function valid_modif(days, hours, mins, secs) {
    return (
        !isNaN(days) && !isNaN(hours) && !isNaN(mins) && !isNaN(secs)
        && (days >= 0 && hours >= 0 && mins >= 0 && secs >= 0)
        && (hours < 24 && mins < 60 && secs < 60)
        && (days + hours + mins + secs > 0)
    )
}

// Quand un site est suivi et qu'on clique sur l'icone pour refaire la vérification
ui.btn_refresh.addEventListener('click', () => {
    // Faire tourner la petite fleche
    ui.animateArrow()
    // Refaire la requête en précisant qu'il s'agit d'une demande manuelle
    port.postMessage({
        hostname: hostname,
        action: 'get_date',
        lim: getCriticalSeconds(hostname),
        manual: true
    })
})
