/**
 * Récupère le nom d'hote d'une URL donnée
 * @param {String} url
 * @returns {String} - Nom d'hôte du serveur
 */
export function getHostname(url) {
    let parser = document.createElement('a')
    parser.href = url
    return parser.hostname
}

/**
 * Donne un tableau contenant les onglets actifs
 * @returns {Promise} - Liste des onglets
 */
export function getTabs() {
    return new Promise(resolve => {
        chrome.tabs.query({ active: true, currentWindow: true }, resolve)
    })
}

export async function getCurrentHostname() {
    let tabs = await getTabs()
    return getHostname(tabs[0].url)
}