import * as storage from './storage'
import { formatDuration } from './date'

// Constantes
const ANIMATION = 'bounceIn'

// Boutons

// Suivre le site
export let btn_follow = document.getElementById('follow')
// Ne plus suivre
export let btn_unfollow = document.getElementById('unfollow')
// Afficher la liste des sites
export let btn_chevron_down = document.getElementById('chevron-down')
// Cacher la liste
export let btn_chevron_up = document.getElementById('chevron-up')
// Vider la liste
export let btn_unfollowall = document.getElementById('unfollowall')
// Confirmer la modification
export let btn_modif_done = document.getElementById('modif_done')
// Refaire la vérification
export let btn_refresh = document.getElementById('refresh')

// Autres éléments

// Le site est suivi
let followed = document.getElementById('followed')
// OCSP Stapling est désactivé
let no_stapling = document.getElementById('no_stapling')
// Le site n'est pas suivi
let unfollowed = document.getElementById('unfollowed')
// Liste des sites suivis
export let sites_list = document.getElementById('sites')
// Champs de modification
let div_modif = document.getElementById('modif_site')
// Informations sur le bouton de la liste
let tip_list = document.getElementById('tip_list_open')
// Action du bouton d'ouverture/fermeture de la liste
let list_action = document.getElementById('list_action')
// Liste des site + bouton de vidage
let list_div = document.getElementById('list_div')

/**
 * Affiche la liste des sites dans un tableau
 */
export function printSites() {
    if (storage.isAvailable()) {
        // Vide l'ancien tableau
        clear()

        let row = sites_list.insertRow(0)

        if (storage.isEmpty()) {
            row.insertCell(0).innerHTML += 'Aucun site suivi.'
            row.insertCell(1).innerHTML += ''
            row.insertCell(2).innerHTML += ''
        } else {
            for (let i = 0; i < storage.getLength(); i++) {
                let hostname = storage.getHostname(i)
                let critical_age = storage.getCriticalSeconds(hostname)
                row.insertCell(0).innerHTML += hostname
                row.insertCell(1).innerHTML += formatDuration(critical_age)
                row.insertCell(2).innerHTML += iconsWithId(hostname)
                row = sites_list.insertRow(0)
            }
        }

        row = sites_list.insertRow(0)
        row.insertCell(0).innerHTML += '<b>Site</b>'
        row.insertCell(1).innerHTML += '<b>Ancienneté critique</b>'
        row.insertCell(2).innerHTML += ''

    } else {
        isUnavailableError()
    }
}

/**
 * Renvoie le string permettant d'ajouter les icônes de modification et de suppression
 */
function iconsWithId(site) {
    return '<i class="fa fa-pencil edit edit_site" id="' + site + '">\</i> <i class="fa fa-remove edit delete_site" id="' + site + '"></i>'
}

/**
 * Vide la tableau
 */
function clear() {
    sites_list.innerHTML = ''
}

/**
 * Ajoute un site dans la liste
 * @param {String} site - nom d'hôte du site à ajouter
 * @param {Number} duree - ancienneté critique en secondes
 */
export function follow(site, duree) {
    storage.addSite(site, duree)
    showFollowed()
    printSites()
}

/**
 * Retire un site de la liste
 * @param {String} site - nom d'hôte du site à retirer
 */
export function unfollow(site) {
    storage.removeSite(site)
    showUnfollowed()
    printSites()
}

/**
 * Retire tous les sites de la liste
 */
export function unfollowAllSites() {
    storage.removeAllSites()
    printSites()
}


/**
 * Afficher l'erreur lors de l'ajout d'une durée invalide
 */
export function showAddWarning() {
    document.getElementById('add_err').hidden = false
}

/**
 * Afficher l'erreur lors de la modification d'une durée invalide
 */
export function showModifWarning() {
    let modif_err = document.getElementById('modif_err')
    modif_err.hidden = false
    modif_err.classList.add('shake')
    setTimeout(() => {
        modif_err.classList.remove('shake')
    }, 1000)
}

/**
 * Affiche la liste des sites
 */
export function showList() {
    btn_chevron_down.hidden = true
    btn_chevron_up.hidden = false
    list_div.hidden = false
    list_action.innerText = "Cacher"
}

/**
 * Cache la liste des sites
 */
export function hideList() {
    btn_chevron_down.hidden = false
    btn_chevron_up.hidden = true
    list_div.hidden = true
    list_action.innerText = "Afficher"
}

export function hideLoading() {
    document.getElementById('loading')
        .hidden = true
}

export function showListButton() {
    document.getElementById('bouton_liste')
        .hidden = false
}

export function showOkIcon() {
    let check = document.getElementById('check')
    check.hidden = false
    setTimeout(() => {
        check.hidden = true
    }, 1000)
}

export function animateArrow() {
    btn_refresh.classList.add('active')
    setTimeout(() => {
        btn_refresh.classList.remove('active')
    }, 1000)
}

/**
 * Affiche que le site est suivi
 * et que l'attestation est dépassée
 * @param {Date} maj - Date de la derniere maj
 * @param {Date} depas - Depassement
 */
export function showDepInfo(maj, depas) {
    // S'assurer que les autres divs soient cachées
    no_stapling.hidden = true
    unfollowed.hidden = true
    div_modif.hidden = true
    // Modification de la div followed
    followed.classList.remove('alert-success')
    followed.classList.add('alert-danger')
    let info_txt = document.getElementById('txt_followed')
    info_txt.innerText = "Ancienneté critique dépassée !"
    info_txt.classList.add(ANIMATION)
    let txt_anc = document.getElementById('txt_anc')
    let txt_dep = document.getElementById('txt_dep')
    let depas_div = document.getElementById('depassement')
    txt_anc.innerText = maj
    txt_dep.innerText = depas
    depas_div.hidden = false
    followed.hidden = false
}

/**
 * Affiche que le site est suivi
 * et que son attestation est correcte
 */
export function showFollowed() {
    // S'assurer que les autres divs soient cachées
    no_stapling.hidden = true
    unfollowed.hidden = true
    div_modif.hidden = true
    let depas_div = document.getElementById('depassement')
    depas_div.hidden = true
    followed.classList.add('alert-success')
    followed.classList.remove('alert-danger')
    let info_txt = document.getElementById('txt_followed')
    info_txt.classList.remove(ANIMATION)
    let txt_anc = document.getElementById('txt_anc')
    info_txt.innerText = "Ce site est suivi"
    // Afficher la div concernee
    followed.hidden = false
}

/**
 * Affiche que le site n'est pas suivi
 */
export function showUnfollowed() {
    followed.hidden = true
    no_stapling.hidden = true
    unfollowed.hidden = false
    div_modif.hidden = true
}

/**
 * Affiche que le site n'utilise pas OCSP Stapling
 */
export function showDisabled() {
    // Cacher toutes les autres divs sauf
    followed.hidden = true
    unfollowed.hidden = true
    div_modif.hidden = true
    no_stapling.hidden = false
}

/**
 * Affiche les champs de modification
 */
export function showModif() {
    // Cacher la div courant (suivi/non suivi)
    followed.hidden = true
    unfollowed.hidden = true
    unfollowed.hidden = true
    div_modif.hidden = false
}

export function showConnectionError() {
    hideLoading()
    document.getElementById('not_connected')
        .hidden = false
}