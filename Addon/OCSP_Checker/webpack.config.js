const path = require('path')

module.exports = {
	entry: {
		background: path.join(__dirname, 'src', 'background.js'),
		popup: path.join(__dirname, 'src', 'popup.js')
	},
	// Fichier compilés en sortie
	output: {
		path: path.join(__dirname, 'dist', 'js'),
		filename: '[name].bundle.js'
	}
}
