# Addon

Installer les dépendances:
```
npm install
```

Compiler en mode développement (compilation continue et log explicites):
```
npm run dev
```


Compiler en mode production (code minifié):
```
npm run build
```

2.

# Pour l'application native

```
./navigateur_install.sh
```