#!/usr/bin/env node

'use strict'

const getUpdate = require('./lib/ocsp_stapling')
const nativeMessaging = require('./lib/native_messaging')
const input = new nativeMessaging.Input();
const transform = new nativeMessaging.Transform(handle_message);
const output = new nativeMessaging.Output()

/**
 * Faire passer le contenu de l'entrée standard
 * en temps qu'objet dans l'application,
 * deleguer le traitement du message à handle_message
 * puis renvoyer le message créer en tant qu'octets
 * précédé par sa longueur sur la sortie standard
 */
process.stdin
    .pipe(input)
    .pipe(transform)
    .pipe(output)
    .pipe(process.stdout)

async function handle_message(msg, push, done) {
	try {
		let date = await getUpdate(msg.hostname)
		push({ update: date })
		done()
	} catch (err) {
		push('SERVER_ERROR')
		done()
	}
}
