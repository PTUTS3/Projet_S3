#!/bin/sh

# Supprimer le json
rm $HOME/.config/google-chrome/NativeMessagingHosts/com.e2.ocsp_checker.json
if [ $? -ne 0 ]; then
	echo "L'extension n'est pas installée sur cette machine";
	exit 1
fi
echo "L'application native a bien été désinstallée"
exit 0

